#include "LinkedList.h"


// 双向带头循环链表
void TestLinkedList()
{
	LinkedListNode* plist = LinkedListInit();
	LinkedListPushFront(plist, 10);
	LinkedListPushFront(plist, 20);
	LinkedListPushFront(plist, 30);
	LinkedListPushFront(plist, 40);
	LinkedListPrint(plist);

	LinkedListNode* pos = LinkedListFind(plist, 30);
	if (pos)
	{
		LinkedListInsert(pos, 666);
	}
	LinkedListPrint(plist);

	LinkedListDestroy(plist);
	plist = NULL;
}
