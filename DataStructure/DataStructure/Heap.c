#include "Heap.h"

void HeapInit(Heap* hp)
{
	assert(hp);
	hp->a = NULL;
	hp->size = hp->capacity = 0;
}

void HeapDestroy(Heap* hp)
{
	assert(hp);
	free(hp->a);
	hp->capacity = hp->size = 0;
}

void HeapPush(Heap* hp, HeapDataType x)
{
	assert(hp);
	// 插入需要检查空间是否足够 是否需要扩容
	if (hp->size == hp->capacity)
	{
		size_t newCapacity = hp->capacity == 0 ? 4 : hp->capacity * 2;
		HeapDataType* tmp = realloc(hp->a, sizeof(HeapDataType) * newCapacity);
		if (tmp == NULL)
		{
			printf("realloc fail\n");
			exit(-1);
		}

		hp->a = tmp;
		hp->capacity = newCapacity;
	}
	// 插入数据到末尾
	hp->a[hp->size] = x;
	hp->size++;

	AdjustUp(hp->a, hp->size - 1);
}

void AdjustUp(int* a, int child)
{
	assert(a);

	// 1.先找到父节点索引
	int parent = (child - 1) / 2;

	while (child > 0)
	{
		// 小根堆向上调整规则
		if (a[child] < a[parent])
		{
			// 2.满足向上调整规则 继续迭代
			Swap(&a[child], &a[parent]);

			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			// 2.已经在合适位置，无需向上调整
			break;
		}
	}
}

// 大根堆向上调整
void AdjustUp_big(int* a, int child)
{
	assert(a);

	int parent = (child - 1) / 2;
	while (child > 0)
	{
		// 大根堆向上调整规则
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);

			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void Swap(HeapDataType* px, HeapDataType* py)
{
	HeapDataType tmp = *px;
	*px = *py;
	*py = tmp;
}

void HeapPrint(Heap* hp)
{
	for (int i = 0; i < hp->size; ++i)
	{
		printf("%d ", hp->a[i]);
	}
	printf("\n");
}

bool HeapEmpty(Heap* hp)
{
	assert(hp);

	return hp->size == 0;
}

int HeapSize(Heap* hp)
{
	assert(hp);

	return hp->size;
}

HeapDataType HeapTop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));

	return hp->a[0];
}

void HeapPop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));

	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;

	AdjustDown(hp->a, hp->size, 0);
}

// 小根堆向下调整
void AdjustDown(int* a, int n, int parent)
{
	int child = parent * 2 + 1;
	// 循环结束条件1是向下调整到叶子结点
	while (child < n)
	{
		// 1.选出左右孩子中小的那一个
		if (child + 1 < n && a[child + 1] < a[child])
		{
			++child;
		}

		// 2.如果小的孩子小于父亲，则交换，并继续向下调整
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			// 循环结束条件2是parent在合适的位置
			break;
		}
	}
}

// 大根堆向下调整
void AdjustDown_big(int* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child] < a[child + 1])
		{
			++child;
		}

		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}