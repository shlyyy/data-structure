#pragma once
#include "common.h"


// 静态的顺序表
// 给小了不够用，给多了浪费
// #define N 10000
// typedef int SeqListDatatype;
// struct SeqList
// {
// 	SeqListDatatype array[N];
// 	int size;
// };

// 动态顺序表
typedef int SeqListDatatype;
typedef struct SeqList
{
	SeqListDatatype* array;
	int size;       // 存储的有效数据的个数
	int capacity;   // 容量
}SeqList;

void SeqListInit(SeqList* psl);
void SeqListDestroy(SeqList* psl);

void SeqListPrint(SeqList* psl);


void SeqListPushBack(SeqList* psl, SeqListDatatype x);
void SeqListPushFront(SeqList* psl, SeqListDatatype x);
void SeqListPopBack(SeqList* psl);
void SeqListPopFront(SeqList* psl);

void SeqListInsert(SeqList* psl, int pos, SeqListDatatype x);
void SeqListErase(SeqList* psl, int pos);

int SeqListFind(SeqList* psl, SeqListDatatype x);
void SeqListModify(SeqList* psl, int pos, SeqListDatatype x);