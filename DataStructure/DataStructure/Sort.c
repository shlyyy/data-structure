#include "Sort.h"

void SwapInt(int* a, int* b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n - 1; ++i)
	{
		bool exchange = false;
		for (int j = 0; j < n - i - 1; ++j)
		{
			if (a[j] > a[j + 1])
			{
				SwapInt(&a[j], &a[j + 1]);
				exchange = true;
			}
		}
		if (!exchange)
		{
			break;
		}
	}
}

void InsertSort(int* a, int n)
{
	for (int i = 0; i < n - 1; ++i)
	{
		// [0, end]已经有序 
		int end = i;
		int tmp = a[i + 1];

		// 将tmp插入到有序序列中
		while (end >= 0)
		{
			// 升序
			if (a[end] > tmp)
			{
				a[end + 1] = a[end];
				--end;
			}
			else
			{
				break;
			}
		}
		// 将tmp插入到合适的位置
		a[end + 1] = tmp;
	}
}

void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		// gap由大变小，最后变为1
		gap = gap / 3 + 1;


		// 一趟gap分组排序 让元素近似有序
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = a[end + gap];

			while (end >= 0)
			{
				// 升序
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;

		}
	}	
}

/*
void SelectSort(int* a, int n)
{
	// n个数需要n-1次选择出无序序列的最值
	for (int i = 0; i < n - 1; ++i)
	{
		// [0, i]是有序序列

		int mini = i;
		for (int j = i + 1; j < n; ++j)
		{
			if (a[j] < a[mini])
			{
				// 记录最值索引
				mini = j;
			}
		}
		// 将最值加入到有序序列中
		if (mini ^ i)
		{
			SwapInt(&a[mini], &a[i]);
		}
	}
}
*/
void SelectSort(int* a, int n)
{
	int begin = 0;
	int end = n - 1;
	while (begin < end)
	{
		int maxi = begin, mini = begin;
		// 记录最值位置
		for (int i = begin; i <= end; i++)
		{
			if (a[i] > a[maxi])
			{
				maxi = i;
			}

			if (a[i] < a[mini])
			{
				mini = i;
			}
		}

		// 最小值加入到有序序列
		if (mini ^ begin)
		{
			SwapInt(&a[begin], &a[mini]);
		}
		
		// 特殊情况：如果maxi和begin重叠，修正一下即可
		if (begin == maxi)
		{
			maxi = mini;
		}

		// 最大值加入到有序序列
		if (maxi ^ end)
		{
			SwapInt(&a[end], &a[maxi]);
		}
		
		++begin;
		--end;
	}
}
