

#include "Heap.h"




// 堆
void TestHeap()
{
	Heap hp;
	HeapInit(&hp);
	int a[] = { 70, 56, 30, 25, 15, 10, 75 };
	for (int i = 0; i < sizeof(a) / sizeof(a[0]); ++i)
	{
		HeapPush(&hp, a[i]);
	}
	HeapPrint(&hp);

	HeapPop(&hp);
	HeapPrint(&hp);

	HeapPop(&hp);
	HeapPrint(&hp);

	HeapPop(&hp);
	HeapPrint(&hp);

	HeapPop(&hp);
	HeapPrint(&hp);

	HeapDestroy(&hp);
}

// 创建堆的两种方式
void CreateHeap()
{
	Heap hp;
	HeapInit(&hp);
	int a[] = { 70, 56, 30, 25, 15, 10, 75 };
	// 向上调整法建立堆
	/*
	for (int i = 0; i < sizeof(a) / sizeof(a[0]); ++i)
	{
		HeapPush(&hp, a[i]);
	}
	HeapPrint(&hp);
	HeapDestroy(&hp);
	*/
	for (int i = 0; i < sizeof(a) / sizeof(a[0]); ++i)
	{
		AdjustUp(a, i);
	}
	for (int i = 0; i < sizeof(a) / sizeof(a[0]); ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");

}
void CreateHeap1()
{
	int a[] = { 70, 56, 30, 25, 15, 10, 75 };
	int n = sizeof(a) / sizeof(a[0]);
	// 向下调整法建立堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}
	for (int i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

// topK问题：在N个数找出最大的前K个  or  在N个数找出最小的前K个
void PrintTopK(int* a, int n, int k)
{
	Heap hp;
	HeapInit(&hp);
	// 创建一个K个数的小堆
	for (int i = 0; i < k; ++i)
	{
		HeapPush(&hp, a[i]);
	}

	// 剩下的N-K个数跟堆顶的数据比较，比他大，就替换他进堆
	for (int i = k; i < n; ++i)
	{
		if (a[i] > HeapTop(&hp))
		{
			HeapPop(&hp);
			HeapPush(&hp, a[i]);
			//hp.a[0] = a[i];
			//AdjustDown(hp.a, hp.size, 0);
		}
	}

	HeapPrint(&hp);

	HeapDestroy(&hp);
}

void PrintTopK1(int* a, int n, int k)
{
	Heap hp;
	HeapInit(&hp);
	/*
	// 1.向下调整建立K个数的大堆
	hp.a = realloc(hp.a, k * sizeof(HeapDataType));
	for (int i = 0; i < k; ++i)
	{
		hp.a[i] = a[i];
		hp.size++;
	}
	for (int i = (k - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown_big(hp.a, k, i);
	}
	*/
	// 1.向上调整建立K个数的大堆
	hp.a = (HeapDataType*)realloc(hp.a, k * sizeof(HeapDataType));
	for (int i = 0; i < k; ++i)
	{
		hp.a[i] = a[i];
		hp.size++;
	}
	for (int i = 0; i < k; ++i)
	{
		AdjustUp_big(hp.a, i);
	}


	// 剩下的N-K个数跟堆顶最大的数据比较，比他小，就替换堆顶数据进堆
	for (int i = k; i < n; ++i)
	{
		if (a[i] < HeapTop(&hp))
		{
			hp.a[0] = a[i];
			AdjustDown_big(hp.a, hp.size, 0);
		}
	}

	HeapPrint(&hp);

	HeapDestroy(&hp);
}

void TestTopk()
{
	/*
	int n = 1000000;
	int* a = (int*)malloc(sizeof(int) * n);
	srand((unsigned int)time(0));
	for (int i = 0; i < n; ++i)
	{
		a[i] = rand() % 1000000;
	}
	// 设置10个比100w大的数
	a[5] = 1000000 + 1;
	a[1231] = 1000000 + 2;
	a[5355] = 1000000 + 3;
	a[51] = 1000000 + 4;
	a[15] = 1000000 + 5;
	a[2335] = 1000000 + 6;
	a[9999] = 1000000 + 7;
	a[76] = 1000000 + 8;
	a[423] = 1000000 + 9;
	a[3144] = 1000000 + 10;
	*/
	int a[] = { 70, 56, 30, 25, 15, 10, 75 };
	int n = sizeof(a) / sizeof(a[0]);
	PrintTopK1(a, n, 3);
}

// 堆排序
// 升序  空间复杂度是多少? O(N) 要求优化到O(1) -> 不能用Heap

void HeapSort1(int* a, int n)
{
	Heap hp;
	HeapInit(&hp);
	// 建议N个元素的小堆
	for (int i = 0; i < n; ++i)
	{
		HeapPush(&hp, a[i]);
	}

	// Pop N 次
	for (int i = 0; i < n; ++i)
	{
		a[i] = HeapTop(&hp);
		HeapPop(&hp);
	}

	HeapDestroy(&hp);
}

void HeapSort2(int* a, int n)
{
	int* arr = a;
	int arrSize = n;

	while (arrSize > 1)
	{
		// 1.向上调整建立小根堆
		for (int i = 1; i < arrSize; ++i)
		{
			AdjustUp(arr, i);
		}
		// 2.数组剩余元素需要重新建立小根堆
		arr += 1;
		arrSize -= 1;
	}
}

// 升序
void HeapSort3(int* a, int n)
{
	// 1.使用向下调整建立大根堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown_big(a, n, i);
	}

	// 2.交换堆顶和末尾 大根堆的堆顶最大值放在数组末尾 重新调堆
	// O(N*logN)
	for (int end = n - 1; end > 0; --end)
	{
		Swap(&a[end], &a[0]);

		// 再调堆，选出次小的数
		AdjustDown_big(a, end, 0);
	}
}

// 降序
void HeapSort4(int* a, int n)
{
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}

	for (int end = n - 1; end > 0; --end)
	{
		Swap(&a[end], &a[0]);

		AdjustDown(a, end, 0);
	}
}

void HeapSortTest()
{
	int a[] = { 70, 56, 30, 25, 15, 10, 75 };
	int n = sizeof(a) / sizeof(a[0]);

	//HeapSort2(a, n);
	HeapSort4(a, n);

	for (int i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}