#include "SeqList.h"

void SeqListInit(SeqList* psl)
{
	assert(psl);
	psl->array = (SeqListDatatype*)malloc(sizeof(SeqListDatatype) * 4);
	if (psl->array == NULL)
	{
		perror("malloc fail");
		return;
	}

	psl->capacity = 4;
	psl->size = 0;
}

void SeqListDestroy(SeqList* psl)
{
	assert(psl);
	free(psl->array);
	psl->array = NULL;
	psl->size = 0;
	psl->capacity = 0;
}

void SeqListPrint(SeqList* psl)
{
	assert(psl);
	for (int i = 0; i < psl->size; i++)
	{
		printf("%d ", psl->array[i]);
	}
	printf("\n");
}

void SeqListCheckCapacity(SeqList* psl)
{
	assert(psl);

	if (psl->size == psl->capacity)
	{
		SeqListDatatype* tmp = (SeqListDatatype*)realloc(psl->array, sizeof(SeqListDatatype) * psl->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}

		psl->array = tmp;
		psl->capacity *= 2;
	}
}

void SeqListPushBack(SeqList* psl, SeqListDatatype x)
{
	assert(psl);

	// 	SeqListCheckCapacity(psl);
	// 
	// 	psl->array[psl->size++] = x;


	SeqListInsert(psl, psl->size, x);
}

void SeqListPushFront(SeqList* psl, SeqListDatatype x)
{
	assert(psl);

	// 	SeqListCheckCapacity(psl);
	// 
	// 	// 挪动数据
	// 	int end = psl->size - 1;
	// 	while (end >= 0)
	// 	{
	// 		psl->array[end + 1] = psl->array[end];
	// 		--end;
	// 	}
	// 
	// 	psl->array[0] = x;
	// 	psl->size++;

	SeqListInsert(psl, 0, x);
}

void SeqListPopBack(SeqList* psl)
{
	assert(psl);
	assert(psl->size > 0);

	//psl->size--;

	SeqListErase(psl, psl->size - 1);
}

void SeqListPopFront(SeqList* psl)
{
	assert(psl);
	assert(psl->size > 0);

	// 	/*int start = 0;
	// 	while (start < psl->size-1)
	// 	{
	// 		psl->array[start] = psl->array[start + 1];
	// 		start++;
	// 	}*/
	// 
	// 	int start = 1;
	// 	while (start < psl->size)
	// 	{
	// 		psl->array[start - 1] = psl->array[start];
	// 		start++;
	// 	}
	// 
	// 	psl->size--;

	SeqListErase(psl, 0);
}

void SeqListInsert(SeqList* psl, int pos, SeqListDatatype x)
{
	assert(psl);
	assert(0 <= pos && pos <= psl->size);

	SeqListCheckCapacity(psl);

	int end = psl->size - 1;
	while (end >= pos)
	{
		psl->array[end + 1] = psl->array[end];
		--end;
	}

	psl->array[pos] = x;
	psl->size++;
}

void SeqListErase(SeqList* psl, int pos)
{
	assert(psl);
	assert(0 <= pos && pos < psl->size);

	int start = pos + 1;
	while (start < psl->size)
	{
		psl->array[start - 1] = psl->array[start];
		++start;
	}

	psl->size--;
}

// 找到返回下标，没有找到返回-1
int SeqListFind(SeqList* psl, SeqListDatatype x)
{
	assert(psl);

	for (int i = 0; i < psl->size; i++)
	{
		if (psl->array[i] == x)
		{
			return i;
		}
	}

	return -1;
}

void SeqListModify(SeqList* psl, int pos, SeqListDatatype x)
{
	assert(psl);
	assert(0 <= pos && pos < psl->size);

	psl->array[pos] = x;
}
