#include "Sort.h"
#include "common.h"

void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void TestBubbleSort()
{
	printf("bubble sort begin\n");
	int a[] = { 4,7,1,9,3,6,5,8,3,2,0 };
	PrintArray(a, sizeof(a) / sizeof(int));
	BubbleSort(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("bubble sort end\n");
}

void TestInsertSort()
{
	printf("insert sort begin\n");
	int a[] = { 4,7,1,9,3,6,5,8,3,2,0 };
	PrintArray(a, sizeof(a) / sizeof(int));
	InsertSort(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("insert sort end\n");
}

void TestShellSort()
{
	printf("shell sort begin\n");
	int a[] = { 4,7,1,9,3,6,5,8,3,2,0 };
	PrintArray(a, sizeof(a) / sizeof(int));
	ShellSort(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("shell sort end\n");
}

void TestSelectSort()
{
	printf("select sort begin\n");
	int a[] = { 4,7,1,9,3,6,5,8,3,2,0 };
	PrintArray(a, sizeof(a) / sizeof(int));
	SelectSort(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
	printf("select sort end\n");
}

void CompareSort()
{
	printf("compare begin\n");
	printf("-----------------------------\n");
	srand((unsigned int)time(0));
	const int N = 100000;
	int* a1 = (int*)malloc(sizeof(int) * N);
	int* a2 = (int*)malloc(sizeof(int) * N);
	int* a3 = (int*)malloc(sizeof(int) * N);
	int* a4 = (int*)malloc(sizeof(int) * N);
	for (int i = 0; i < N; ++i)
	{
		a1[i] = rand();
		a2[i] = a1[i];
		a3[i] = a1[i];
		a4[i] = a1[i];
	}

	int begin1 = clock();
	BubbleSort(a1, N);
	int end1 = clock();

	int begin2 = clock();
	InsertSort(a2, N);
	int end2 = clock();

	int begin3 = clock();
	ShellSort(a3, N);
	int end3 = clock();

	int begin4 = clock();
	SelectSort(a4, N);
	int end4 = clock();

	printf("BubbleSort:%d\n", end1 - begin1);
	printf("InsertSort:%d\n", end2 - begin2);
	printf("ShellSort:%d\n", end3 - begin3);
	printf("SelectSort:%d\n", end4 - begin4);

	printf("-----------------------------\n");
	printf("compare end\n");
}

void TestSort()
{
// 	TestBubbleSort();
// 	TestInsertSort();
// 	TestShellSort();
// 	TestSelectSort();

	CompareSort();
}

