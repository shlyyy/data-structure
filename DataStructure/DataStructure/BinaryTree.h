#pragma once
#include "common.h"

typedef char BTDataType;
typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
	BTDataType data;
}BTNode;


BTNode* CreatBinaryTree();

// 遍历
void PreOrder(BTNode* root);
void InOrder(BTNode* root);
void PostOrder(BTNode* root);
void LevelOrder(BTNode* root);

// 二叉树的结点个数
int BinaryTreeSize(BTNode* root);

// 二叉树的叶子结点个数
int BinaryTreeLeafSize(BTNode* root);

// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k);

// 二叉树的深度/高度
int BinaryTreeDepth(BTNode* root);

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x);

// 965.单值二叉树   https://leetcode.cn/problems/univalued-binary-tree/
struct TreeNode {
	int val;
	struct TreeNode* left;
	struct TreeNode* right;
};
bool isUnivalTree(struct TreeNode* root);

// 144. 二叉树的前序遍历   https://leetcode.cn/problems/binary-tree-preorder-traversal/
int* preorderTraversal(struct TreeNode* root, int* returnSize);

// 100.相同的树   https://leetcode.cn/problems/same-tree/
bool isSameTree(struct TreeNode* p, struct TreeNode* q);

// 101.对称的树   https://leetcode.cn/problems/symmetric-tree/
bool isSymmetric(struct TreeNode* root);

// 572.另一棵树的子树   https://leetcode.cn/problems/subtree-of-another-tree/
bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot);



// 二叉树的销毁
void BinaryTreeDestroy(BTNode* root);


// 判断二叉树是否是完全二叉树
bool BinaryTreeComplete(BTNode* root);


