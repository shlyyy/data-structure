#include "BinaryTree.h"


BTNode* CreateNode(BTDataType x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	if (node == NULL)
	{
		printf("malloc fail\n");
		exit(-1);
	}

	node->data = x;
	node->left = node->right = NULL;
	return node;
}

BTNode* CreatBinaryTree()
{
	/*
				A
		B				C
	D				E		F
	*/
	BTNode* nodeA = CreateNode('A');
	BTNode* nodeB = CreateNode('B');
	BTNode* nodeC = CreateNode('C');
	BTNode* nodeD = CreateNode('D');
	BTNode* nodeE = CreateNode('E');
	BTNode* nodeF = CreateNode('F');

	nodeA->left = nodeB;
	nodeA->right = nodeC;
	nodeB->left = nodeD;
	nodeC->left = nodeE;
	nodeC->right = nodeF;

	return nodeA;
}

// 二叉树前序遍历
void PreOrder(BTNode* root)
{
	if (root == NULL)
	{
		//printf("NULL ");
		return;
	}

	printf("%c ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}

void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		//printf("NULL ");
		return;
	}

	InOrder(root->left);
	printf("%c ", root->data);
	InOrder(root->right);
}

void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		//printf("NULL ");
		return;
	}

	PostOrder(root->left);
	PostOrder(root->right);
	printf("%c ", root->data);
}


#include "Queue.h"
void LevelOrder(BTNode* root)
{
	assert(root);

	// 需要修改下面使用到的队列中存储的元素类型为节点指针
	/*
	Queue q;
	QueueInit(&q);

	QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);

		printf("%c ", front->data);

		if(root->left)
			QueuePush(&q, root->left);
		if(root->right)
			QueuePush(&q, root->right);
	}

	QueueDestroy(&q);
	*/
}


// 二叉树节点个数
/*
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}

	// 遍历计数思想：多次调用存在问题
	static int count = 0;

	++count;
	BinaryTreeSize(root->left);
	BinaryTreeSize(root->right);

	return count;
}

void BinaryTreeSize(BTNode* root, int* pn)
{
	if (root == NULL)
	{
		return;
	}

	++(*pn);
	BinaryTreeSize(root->left, pn);
	BinaryTreeSize(root->right, pn);
}
*/

int BinaryTreeSize(BTNode* root)
{
	/*
	if (root == NULL)
	{
		return 0;
	}
	return 1 + BinaryTreeSize(root->left) + BinaryTreeSize(root->right);
	*/
	return root == NULL ? 0 : BinaryTreeSize(root->left) + BinaryTreeSize(root->right) + 1;
}


// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	// 如果是叶子结点
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}

	// 左右子树叶子结点的和
	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}

// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	assert(k >= 1);

	if (root == NULL)
	{
		return 0;
	}

	// 找到第k层结点
	if (k == 1)
	{
		return 1;
	}

	// 左右子树第k-1层结点的和
	return BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);
}

// 二叉树的深度/高度
int BinaryTreeDepth(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	int leftDepth = BinaryTreeDepth(root->left);
	int rightDepth = BinaryTreeDepth(root->right);
	// 左右子树高度的最大值加1
	return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
}

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}

	if (root->data == x)
	{
		return root;
	}

	
	BTNode* leftRet = BinaryTreeFind(root->left, x);
	if (leftRet)
	{
		return leftRet;
	}

	BTNode* rightRet = BinaryTreeFind(root->right, x);
	if (rightRet)
	{
		return rightRet;
	}

	return NULL;
}


bool isUnivalTree(struct TreeNode* root) {
	if (root == NULL)
	{
		return true;
	}

	// 左子树存在 根节点和左子树比较
	if (root->left && root->left->val != root->val)
	{
		return false;
	}
	// 右子树存在 根节点和右子树比较
	if (root->right && root->right->val != root->val)
	{
		return false;
	}
	// root结点跟左右结点是单值
	// 递归判断root左子树和右子树是否同时是单值二叉树
	return isUnivalTree(root->left) && isUnivalTree(root->right);
}


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
void _preorderTraversal(struct TreeNode* root, int* preorder, int* pIndex)
{
	if (root == NULL)
	{
		return;
	}
	
	printf("%d ", root->val);
	preorder[(*pIndex)++] = root->val;
	_preorderTraversal(root->left, preorder, pIndex);
	_preorderTraversal(root->right, preorder, pIndex);
}
int* preorderTraversal(struct TreeNode* root, int* returnSize) {
	// 得到树中结点总数：一方面作为retrunSize返回值，另一方面作为下面申请空间大小的依据
	int size = BinaryTreeSize(root);
	*returnSize = size;
	printf("size:%d\n", size);

	int* preorder = (int*)malloc(size * sizeof(int));
	int index = 0;
	_preorderTraversal(root, preorder, &index);
	return preorder;
}


bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
	// 两颗空树是相同的树
	if (p == NULL && q == NULL)
	{
		return true;
	}

	// 如果有一个为空，另一个不为空
	if (p == NULL || q == NULL)
	{
		return false;
	}

	// 如果值不相等
	if (p->val != q->val)
	{
		return false;
	}

	// p与q的左子树，右子树都要是相同的树
	return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
}


bool _isSymmetric(struct TreeNode* leftRoot, struct TreeNode* rightRoot) {
	// 两个都为空
	if (leftRoot == NULL && rightRoot == NULL)
	{
		return true;
	}

	// 其中一个为空，另一个不为空
	if (leftRoot == NULL || rightRoot == NULL)
	{
		return false;
	}

	// 都不为空，比较值
	if (leftRoot->val != rightRoot->val)
	{
		return false;
	}

	// leftRoot的左子树与rightRoot的右子树
	// leftRoot的右子树与rightRoot的左子树
	return _isSymmetric(leftRoot->left, rightRoot->right) && _isSymmetric(leftRoot->right, rightRoot->left);
}
bool isSymmetric(struct TreeNode* root) {
	return _isSymmetric(root->left, root->right);
}


bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) {
	// subRoot不为空，root为空直接false
	if (root == NULL)
	{
		return false;
	}

	// 如果是相同的树
	if (isSameTree(root, subRoot))
	{
		return true;
	}

	// root的左右子树中找subRoot子树
	return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
}



void BinaryTreeDestroy(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}

	BinaryTreeDestroy(root->left);
	BinaryTreeDestroy(root->right);
	free(root);
}


bool BinaryTreeComplete(BTNode* root)
{
	// 需要修改下面使用到的队列中存储的元素类型为节点指针
	/*
	Queue q;
	QueueInit(&q);

	if (root)
		QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);

		// 遇到第一个空结点跳出
		if(front == NULL)
			break;

		QueuePush(&q, root->left);
		QueuePush(&q, root->right);
	}

	// 检查队列中还有没有非空节点，有非空节点就不是完全二叉树
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);

		if (front)
		{
			QueueDestroy(&q);
			return false;
		}
	}

	QueueDestroy(&q);
	*/
	return true;
}
