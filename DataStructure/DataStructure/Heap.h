#pragma once
#include "common.h"


typedef int HeapDataType;
typedef struct Heap
{
	HeapDataType* a;
	int size;
	int capacity;
}Heap;

void HeapInit(Heap* hp);
void HeapDestroy(Heap* hp);


void HeapPush(Heap* hp, HeapDataType x);
void AdjustUp(int* a, int child);
void AdjustUp_big(int* a, int child);
void Swap(HeapDataType* px, HeapDataType* py);

void HeapPrint(Heap* hp);

bool HeapEmpty(Heap* hp);
int HeapSize(Heap* hp);
HeapDataType HeapTop(Heap* hp);


void HeapPop(Heap* hp);
void AdjustDown(int* a, int n, int parent);
void AdjustDown_big(int* a, int n, int parent);
