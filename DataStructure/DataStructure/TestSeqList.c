
#include "SeqList.h"



// ˳���
void TestSeqList()
{
	SeqList sl;
	SeqListInit(&sl);

	SeqListPushBack(&sl, 1);
	SeqListPushBack(&sl, 2);
	SeqListPushBack(&sl, 3);
	SeqListPushBack(&sl, 4);
	SeqListPushBack(&sl, 5);
	SeqListPushBack(&sl, 6);
	SeqListPrint(&sl); // 1 2 3 4 5 6

	SeqListPopFront(&sl);
	SeqListPrint(&sl); // 2 3 4 5 6

	int pos = SeqListFind(&sl, 6);
	if (pos != -1)
	{
		SeqListErase(&sl, pos);
	}
	SeqListPrint(&sl); // 2 3 4 5

	SeqListDestroy(&sl);
}