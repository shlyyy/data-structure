#include "Stack.h"

void STInit(ST* pst)
{
	assert(pst);
	pst->data = NULL;

	//pst->top = -1;   // top 指向栈顶数据
	pst->top = 0;   // top 指向栈顶数据的下一个位置

	pst->capacity = 0;
}

void STDestroy(ST* pst)
{
	assert(pst);

	free(pst->data);
	pst->data = NULL;
	pst->top = pst->capacity = 0;
}

void STPush(ST* pst, STDataType x)
{
	assert(pst);

	// 插入检查是否需要扩容
	if (pst->top == pst->capacity)
	{
		int newCapacity = pst->capacity == 0 ? 4 : pst->capacity * 2;
		STDataType* tmp = (STDataType*)realloc(pst->data, newCapacity * sizeof(STDataType));
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}

		pst->data = tmp;
		pst->capacity = newCapacity;
	}

	pst->data[pst->top] = x;
	pst->top++;
}

void STPop(ST* pst)
{
	assert(pst);
	assert(!STEmpty(pst));

	pst->top--;
}

STDataType STTop(ST* pst)
{
	assert(pst);
	assert(!STEmpty(pst));

	return pst->data[pst->top - 1];
}

bool STEmpty(ST* pst)
{
	assert(pst);

	return pst->top == 0;
}

int STSize(ST* pst)
{
	assert(pst);

	return pst->top;
}



// https://leetcode.cn/problems/valid-parentheses/
bool isValid(char* s) {
	ST st;
	STInit(&st);
	while (*s)
	{

		if (*s == '(' ||
			*s == '[' ||
			*s == '{')
		{
			// 1.左括号入栈
			STPush(&st, *s);
		}
		else
		{
			// 如果是右括号，且栈为空，则销毁栈，返回false
			if (STEmpty(&st))
			{
				STDestroy(&st);
				return false;
			}

			// 2.右括号出栈匹配
			char top = STTop(&st);
			STPop(&st);
			if ((*s == ']' && top != '[') ||
				(*s == ')' && top != '(') ||
				(*s == '}' && top != '{'))
			{
				STDestroy(&st);
				return false;
			}
		}
		++s;
	}

	// 如果字符遍历完毕，栈不为空，则返回false
	bool ret = STEmpty(&st);
	STDestroy(&st);
	return ret;
}