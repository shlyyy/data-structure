#include "SingleLinkedListWithLeadingNode.h"



// 带头结点单链表
void TestSLLWithLeadingNode()
{
	SLLWNode* plist = NULL;
	SLLWInit(&plist);

	SLLWPushBack(plist, 10);
	SLLWPushBack(plist, 20);
	SLLWPrint(plist);
	SLLWPushFront(plist, 30);
	SLLWPushFront(plist, 40);
	SLLWPrint(plist);

	SLLWPopFront(plist);
	SLLWPrint(plist);

	SLLWPopBack(plist);
	SLLWPrint(plist);

	SLLWNode* del = SLLWFind(plist, 10);
	SLLWErase(plist, del);
	SLLWPrint(plist);


	SLLWDestroy(plist);
}

