#include "BinaryTree.h"

/*
			A
	B				C
D				E		F

前序：A B D C E F
中序：D B A E C F
后序：D B E F C A
*/
// 二叉树遍历
void TraverseBinaryTree()
{
	BTNode* root = CreatBinaryTree();

	PreOrder(root);
	printf("\n");

	InOrder(root);
	printf("\n");

	PostOrder(root);
	printf("\n");
}





void TestBinaryTree()
{
	//TraverseBinaryTree();

	//BTNode* root = CreatBinaryTree();
	//printf("%d\n", BinaryTreeSize(root));
	
	//BTNode* root = CreatBinaryTree();
	//printf("%d\n", BinaryTreeLeafSize(root));

	//BTNode* root = CreatBinaryTree();
	//printf("%d\n", BinaryTreeLevelKSize(root, 2));

	//BTNode* root = CreatBinaryTree();
	//printf("%d\n", BinaryTreeDepth(root));

	BTNode* root = CreatBinaryTree();
	BTNode* target = BinaryTreeFind(root, 'E');
	printf("%c\n", target == NULL ? 0 : target->data);
}

