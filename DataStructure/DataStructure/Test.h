#pragma once
#include "common.h"

void TestSeqList();
void TestSLL();
void TestSLLWithLeadingNode();
void TestLinkedList();

void TestStack();

void TestHeap();
void CreateHeap();
void CreateHeap1();
void PrintTopK(int* a, int n, int k);
void TestTopk();
void HeapSortTest();

void TestBinaryTree();
void TraverseBinaryTree();

void TestSort();
void CompareSort();


