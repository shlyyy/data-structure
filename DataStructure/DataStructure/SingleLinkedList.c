#include "SingleLinkedList.h"

void SLLPrint(SLLNode* phead)
{
	SLLNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}

	printf("NULL\n");
}

SLLNode* CreateSLLNode(SLLDataType x)
{
	SLLNode* newnode = (SLLNode*)malloc(sizeof(SLLNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

void SLLPushFront(SLLNode** pphead, SLLDataType x)
{
	assert(pphead);  // 链表为空，pphead也不为空，因为他是头指针plist的地址
	//assert(*pphead); // 不能断言，链表为空，也需要能插入

	SLLNode* newnode = CreateSLLNode(x);

	newnode->next = *pphead;
	*pphead = newnode;
}

/*
void SLLPushBack(SLLNode* phead, SLLDataType x)
{
	SLLNode* tail = phead;
	while (tail != NULL)
	{
		tail = tail->next;
	}

	SLLNode* newnode = CreateSLLNode(x);
	tail = newnode;
}


void SLLPushBack(SLLNode* phead, SLLDataType x)
{
	SLLNode* tail = phead;
	while (tail->next != NULL)
	{
		tail = tail->next;
	}

	SLLNode* newnode = CreateSLLNode(x);
	tail->next = newnode;
}
*/


void SLLPushBack(SLLNode** pphead, SLLDataType x)
{
	assert(pphead); // 链表为空，pphead也不为空，因为他是头指针plist的地址
	//assert(*pphead); // 链表为空，可以尾插

	SLLNode* newnode = CreateSLLNode(x);
	
	if (*pphead == NULL)
	{
		// 1、空链表
		*pphead = newnode;
	}
	else
	{
		// 2、非空链表
		SLLNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}

		tail->next = newnode;
	}
}

void SLLPopFront(SLLNode** pphead)
{
	assert(pphead); // 链表为空，pphead也不为空
	assert(*pphead); // 链表为空，不能头删。

	SLLNode* del = *pphead;
	*pphead = (*pphead)->next;
	free(del);

	/*
	if ((*pphead)->next == NULL)
	{
		// 一个节点
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		// 多个节点
		SLLNode* del = *pphead;
		//*pphead = del->next;
		*pphead = (*pphead)->next;
		free(del); 
	}
	*/
}

void SLLPopBack(SLLNode** pphead)
{
	assert(pphead); // 链表为空，pphead也不为空，因为他是头指针plist的地址
	assert(*pphead); // 链表为空，不能头删。（当然你还可以用温柔的检查）
	// 温柔的检查
	/*if (*pphead == NULL)
	{
		return;
	}*/

	if ((*pphead)->next == NULL)
	{
		// 1.链表中只有一个节点
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		// 2.链表中有多个节点
		/*
		SLLNode* prev = NULL;
		SLLNode* tail = *pphead;
		// 找尾
		while (tail->next)
		{
			prev = tail;
			tail = tail->next;
		}

		free(tail);
		prev->next = NULL;
		*/
		SLLNode* tail = *pphead;
		// 找尾
		while (tail->next->next)
		{
			tail = tail->next;
		}

		free(tail->next);
		tail->next = NULL;
	}
}


SLLNode* SLLFind(SLLNode* phead, SLLDataType x)
{
	//assert(phead);

	SLLNode* cur = phead;
	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}

		cur = cur->next;
	}

    return NULL;
}

void SLLInsert(SLLNode** pphead, SLLNode* pos, SLLDataType x)
{
	assert(pphead);
	assert(pos);

	SLLNode* prev = *pphead;
	if (prev == pos)
	{
		SLLPushFront(pphead, x);
	}
	else
	{
		// The pos node is not found, a null pointer exception is raised
		while (prev->next != pos)
		{
			prev = prev->next;
		}

		SLLNode* newnode = CreateSLLNode(x);
		prev->next = newnode;
		newnode->next = pos;
	}
}

void SLLInsert1(SLLNode** pphead, SLLNode* pos, SLLDataType x)
{
	assert(pphead);
	assert(pos);

	// 这里不需要判断pos是否在链表中
	// 因为从使用者的角度来看，一般是先Find找到x所在的pos
	// 然后Insert插入到找到的pos的位置之前。
	// 所以pos必在链表中

	SLLNode* newnode = CreateSLLNode(pos->data);
	pos->data = x;
	newnode->next = pos->next;
	pos->next = newnode;
}

void SLLInsertAfter(SLLNode* pos, SLLDataType x)
{
	assert(pos);

	SLLNode* newnode = CreateSLLNode(x);
	newnode->next = pos->next;
	pos->next = newnode;
}

void SLLErase(SLLNode** pphead, SLLNode* pos)
{
	assert(pphead);
	assert(pos);

	SLLNode* prev = *pphead;
	if (pos == prev)
	{
		SLLPopFront(pphead);
	}
	else
	{
		while (prev->next != pos)
		{
			prev = prev->next;
		}

		prev->next = pos->next;
		free(pos);
	}
}

void SLLEraseAfter(SLLNode* pos)
{
	assert(pos);
	assert(pos->next);

	SLLNode* next = pos->next;
	pos->next = next->next;
	free(next);
}

void SLLDestroy(SLLNode* phead)
{
	SLLNode* cur = phead;
	while (cur)
	{
		SLLNode* del = cur;
		cur = cur->next;
		free(del);
	}
}
