#pragma once
#include "common.h"

typedef int SLLDataType;
typedef struct SLLNode
{
	SLLDataType data;
	struct SLLNode* next;
}SLLNode;

void SLLPrint(SLLNode* phead);

void SLLPushFront(SLLNode** pphead, SLLDataType x);
void SLLPushBack(SLLNode** pphead, SLLDataType x);

void SLLPopFront(SLLNode** pphead);
void SLLPopBack(SLLNode** pphead);

SLLNode* SLLFind(SLLNode* phead, SLLDataType x);

// 在pos之前插入
void SLLInsert(SLLNode** pphead, SLLNode* pos, SLLDataType x);
// 在pos之后插入
void SLLInsertAfter(SLLNode* pos, SLLDataType x);

// 删除pos位置的值
void SLLErase(SLLNode** pphead, SLLNode* pos);
// 删除pos位置后面的值
void SLLEraseAfter(SLLNode* pos);

// 销毁
void SLLDestroy(SLLNode* phead);
